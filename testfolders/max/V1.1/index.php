<html>
	<head>

		<link rel="Stylesheet" type="text/css" href="stylesheets/style.css" />

		<script src="javascript/jquery-1.4.4.min.js" type="text/javascript"></script>
		<script src="javascript/jpicker-1.1.6.min.js" type="text/javascript"></script>
		<script>
			var selected = null;
			var size_select = null;
			 $.fn.extend({ 
                disableSelection: function() { 
                    this.each(function() { 
                        if (typeof this.onselectstart != 'undefined') {
                            this.onselectstart = function() { return false; };
                        } else if (typeof this.style.MozUserSelect != 'undefined') {
                            this.style.MozUserSelect = 'none';
                        } else {
                            this.onmousedown = function() { return false; };
                        }
                    }); 
                } 
            });
	<?php 
		if($_GET['error']=='description')
			echo "alert('ERROR: Description too long!');";
		?>
		</script>
		
	</head>
		<title>Infographic Maker V1.1</title>

	<body>

		<div class="container">
		<center><h1 style="font-size: 30px; width: 100%;">Infographic Maker V1.1</h1><br></center>
		<form action="PHP/generate_header.php" method="post">
			<h1 style="float:left; margin-left: 115px;">Title</h1> <h1 style="float:left; margin-left: 240px;">Author</h1>
			<br><br>
			<input class="bigbox" <?php echo "value = '" . $_GET['title'] . "'"; ?> style="margin-left: 25px;" name="title">
			
			<input class="bigbox" <?php echo "value = '" . $_GET['author'] . "'";  ?> style="margin-left: 50px;" name="author">
			<br><br>
			<center><h1> Description</h1></center>
			<textarea name="description" class="bigbox"> <?php echo $_GET['description']; ?> </textarea>
			<br><br>
			<h1>Select a Width</h1>
			<br>
			<div class="size_box" id="small" name="small">
					<h2 style="font-size: 15px;" class="size_select">Small</h2>
			</div>
			<div class="size_box" id="medium" name="medium">
					<h2 class="size_select">Medium</h2>
			</div>
			<div class="size_box" id="large" name="large">
					<h2 style="font-size:25px;" class="size_select">Large</h2>
			</div>
			<br><br><br><br><br>
			
			<h1>Select a color pallet</h1>
			
			<div class="pallet_box">
				<div class="pallet" id="underground">
					<h2>Underground</h2>
					<div class="pallet_color" style="background-color: #E3CBAC;"> </div>
					<div class="pallet_color" style="background-color: #9C9985;"> </div>
					<div class="pallet_color" style="background-color: #C46D3B;"> </div>
				</div>
				<div class="pallet" id="clownfish">
					<h2>Clownfish</h2>
					<div class="pallet_color" style="background-color: #3E5916;"> </div>
					<div class="pallet_color" style="background-color: #93A605;"> </div>
					<div class="pallet_color" style="background-color: #F28705;"> </div>
					<div class="pallet_color" style="background-color: #F25C05;"> </div>
				</div>
				<br>
				<div class="pallet" id="wine">
					<h2>Wine</h2>
					<div class="pallet_color" style="background-color: #47282C;"> </div>
					<div class="pallet_color" style="background-color: #8C8468;"> </div>
					<div class="pallet_color" style="background-color: #C9B37F;"> </div>
					<div class="pallet_color" style="background-color: #D8DAB7;"> </div>
				</div>
				<div class="pallet" id="dribble">
					<h2>Dribble</h2>
					<div class="pallet_color" style="background-color: #3D4C53;"> </div>
					<div class="pallet_color" style="background-color: #70B7BA;"> </div>
					<div class="pallet_color" style="background-color: #F1433F;"> </div>
					<div class="pallet_color" style="background-color: #E7E1D4;"> </div>
				</div>
				
				<div class="pallet" id="black_and_white">
					<h2>B&W</h2>
					<div class="pallet_color" style="background-color: #000000;"> </div>
					<div class="pallet_color" style="background-color: #FFFFFF;"> </div>
				</div>
			</div>
				<input style="display: none;" id="size" class="pallet_input" name="size"><br>
				<input style="display: none;" id="pallet" class="pallet_input" name="pallet"><br>
				<input class="submit" type="submit">
			</form>

			
		</div>
		<script src="javascript/buttonevents.js" type="text/javascript"></script>
	</body>

</html>