<?php

	//Color one: background color
	//Color two: Main headers
	//Color three: Sub headers
	//Color four: Body text
	
	$underground_text=array("#E3CBAC", "#324654", "#9C9985", "#788880");
	$underground_graphics=array("#E3CBAC", "#9C9985", "#C46D3B", "#788880", "#324654");

	$clownfish_text=array("#F25C05", "#E5EFFA", "#A63D00", "#FFCEB1");
	$clownfish_graphics=array("#3E5916", "#93A605", "#F28705", "#F25C05", "#E5EFFA");
	
	$wine_text=array("#DBDAB7", "#47282C", "#8C8468", "#604649");
	$wine_graphics=array("#47282C", "#8C8468", "#C9B37F", "#D8DAB7", "#C4C49C");
	
	$dribble_text=array("#70B7BA", "#F1433F", "#3D4C53", "#604649");
	$dribble_graphics=array("#3D4C53", "#70B7BA", "#F1433F", "#E7E1D4", "#FFFFFF");
	
	$black_and_white_text=array("#FFFFFF", "#000000", "#000000", "#000000");
	$black_and_white_graphics=array("#000000", "#000000", "#000000", "#000000", "#000000");
	
	$small="600";
	
	$medium="800";
	
	$large="1000";
?>