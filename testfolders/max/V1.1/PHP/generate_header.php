<?php
	//libraries
	require_once("gradient-fill.php"); //needed to make the gradients
	require_once("colors.php"); //pallet and size data

	//variables 
	$pallet = $_POST['pallet'];
	$size = $_POST['size'];
	$title = $_POST['title'];
	$author = "By: " . $_POST['author'];
	$description = $_POST['description'];
	$description_max_len = 70;
	$author_font_adjuster = 5;
	$title_font_adjuster = 10;
	$max_author_font = 15;
	$max_title_font = 45;
	$body_font_adjuster = 4;
	$width;
	$bgcolor_reference;
	$main_header_color_ref;
	$sub_header_color_ref;
	$body_text_color_ref;
	$font_path = "../fonts/Freeroad.ttf";
	$color2;
	switch($size){
		case "small": $width = $small;
						break;
		case "medium": $width = $medium;
						break;
		case "large": $width = $large;
						break;
	}

	switch($pallet){
		case "underground": $bgcolor_reference = $underground_text[0];
							$main_header_color_ref = $underground_text[1];
							$sub_header_color_ref = $underground_text[2];
							$body_text_color_ref = $underground_text[3];
							break;
		case "clownfish": $bgcolor_reference = $clownfish_text[0];
						  $main_header_color_ref = $clownfish_text[1];
						  $sub_header_color_ref = $clownfish_text[2];
						  $body_text_color_ref = $clownfish_text[3];
						  break;
		case "wine": $bgcolor_reference = $wine_text[0];
					 $main_header_color_ref = $wine_text[1];
					 $sub_header_color_ref = $wine_text[2];
					 $body_text_color_ref = $wine_text[3];
					 break;
		case "dribble": $bgcolor_reference = $dribble_text[0];
						$main_header_color_ref = $dribble_text[1];
						$sub_header_color_ref = $dribble_text[2];
						$body_text_color_ref = $dribble_text[3];
						break;
		case "black_and_white": $bgcolor_reference = $black_and_white_text[0];
						$main_header_color_ref = $black_and_white_text[1];
						$sub_header_color_ref = $black_and_white_text[2];
						$body_text_color_ref = $black_and_white_text[3];
						break;
	}



	$bgcolor_referencergb = hex2rgb($bgcolor_reference);
	$main_header_color_ref_rgb = hex2rgb($main_header_color_ref);
	$sub_header_color_ref_rgb = hex2rgb($sub_header_color_ref);
	$body_text_color_ref_rgb = hex2rgb($body_text_color_ref);


	if($bgcolor_referencergb[0] + 50 > 255)
		$bgcolor_referencergb[0] = 255;
	else
		$bgcolor_referencergb[0]=$bgcolor_referencergb[0]+50;

	if($bgcolor_referencergb[1] + 50 > 255)
		$bgcolor_referencergb[1] = 255;
	else
		$bgcolor_referencergb[1]=$bgcolor_referencergb[1]+50;
			

	if($bgcolor_referencergb[2] + 50 > 255)
		$bgcolor_referencergb[2] = 255;
	else
		$bgcolor_referencergb[2]=$bgcolor_referencergb[2]+50;
			
		
	$color2=rgb2hex($bgcolor_referencergb);


	$gradient_spawn = new gd_gradient_fill($width,100,'vertical',$color2,$bgcolor_reference);
	 
	 

	$bgcolor_referencergb = hex2rgb($bgcolor_reference); 
	 
	header('content-type: image/png');


	//Create background and copy it to the gradient
	$gradient = imagecreatefrompng('gradient.png');

	if($width == 600)
		$background = imagecreate($width, 150);
	if($width == 800)
		$background = imagecreate($width, 170);
	if($width == 1000)
		$background = imagecreate($width, 90);
	
	$backgroundcolor = imagecolorallocate($background, $bgcolor_referencergb[0], $bgcolor_referencergb[1], $bgcolor_referencergb[2]);
	$bgcolor = imagecolorallocate($background, $bgcolor_reference_explode[0], $bgcolor_reference_explode[1], $bgcolor_reference_explode[2]); 
	imagecopymerge($background, $gradient, 0, 0, 0, 0, $width, 100, 100);

	$main_header_color = imagecolorallocate($background, $main_header_color_ref_rgb[0], $main_header_color_ref_rgb[1], $main_header_color_ref_rgb[2]);
	$sub_header_color = imagecolorallocate($background, $sub_header_color_ref_rgb[0], $sub_header_color_ref_rgb[1], $sub_header_color_ref_rgb[2]);
	$body_text_color = imagecolorallocate($background, $body_text_color_ref_rgb[0], $body_text_color_ref_rgb[1], $body_text_color_ref_rgb[2]);

	//Set some title and author font sizes so stuff doesn't go off the screen or anything
		$title_font_size = (350/strlen($title))+$title_font_adjuster;
		$author_font_size =(180/strlen($author));
		if($author_font_size > $max_author_font)
			$author_font_size = $max_author_font;
		if($title_font_size > $max_title_font)
			$title_font_size = $max_title_font;
	if($width == 600){
		$description_max_len-=3;
		if(strlen($description) > $description_max_len * 4)
			header("Location: ../index.php?error=description&description=" . $_POST['description'] . "&author=" . $_POST['author'] . "&title=" . $_POST['title']);

		//Print the title
		imagettftext($background, $title_font_size, 0, 20, 70, $main_header_color, $font_path, $title);
		//Print the author
		imagettftext($background, $author_font_size, 0, 410+(($author_font_size/strlen($author)))*strlen($author), 22, $sub_header_color, $font_path, $author);

		//Set up a description buffer to break up the lines
		//Line 1
		$description_buffer = substr($description, 0, $description_max_len);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len] != ' ' && strlen($description) > $description_max_len)
			$description_buffer = $description_buffer . "-";
		imagettftext($background, 14-$body_font_adjuster, 0, 20, 90, $body_text_color, $font_path, $description_buffer);

		//Line 2
		$description_buffer = substr($description, $description_max_len, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*2] != ' ' && $description_max_len*2 < strlen($description))
			$description_buffer = $description_buffer . "-";
		imagettftext($background, 14-$body_font_adjuster, 0, 20, 95+14-$body_font_adjuster, $body_text_color, $font_path, $description_buffer);

		//Line 3
		$description_buffer = substr($description, $description_max_len*2, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*3] != ' ' && $description_max_len*3 < strlen($description))
			$description_buffer = $description_buffer . "-";
		imagettftext($background, 14-$body_font_adjuster, 0, 20, 100+2*(14-$body_font_adjuster), $body_text_color, $font_path, $description_buffer);

		//Line 4
		$description_buffer = substr($description, $description_max_len*3, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		imagettftext($background, 14-$body_font_adjuster, 0, 20, 105+3*(14-$body_font_adjuster), $body_text_color, $font_path, $description_buffer);
	}
	
	if($width == 800){
	
		$description_max_len = (1-(600/800)+1)*$description_max_len;
		
		if(strlen($description) > $description_max_len * 4)
			header("Location: ../index.php?error=description&description=" . $_POST['description'] . "&author=" . $_POST['author'] . "&title=" . $_POST['title']);
			
		$title_y = $title_font_size + 10 +$title_font_adjuster; 
		$bbox = imagettfbbox($title_font_size+$title_font_adjuster, 0, $font_path, $title);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;

		// Write it
		imagettftext($background, $title_font_size+$title_font_adjuster, 0, $x, $title_y, $main_header_color, $font_path, $title);
		
		$bbox = imagettfbbox($author_font_size+$title_font_adjuster, 0, $font_path, $author);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		$author_y = $title_y + $author_font_size + 10 +$title_font_adjuster;
		// Write it
		imagettftext($background, $author_font_size+$title_font_adjuster, 0, $x, $author_y, $sub_header_color, $font_path, $author);
		
		
		//Line 1
		$description_buffer = substr($description, 0, $description_max_len);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len] != ' ' && strlen($description) > $description_max_len)
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox(14-$body_font_adjuster, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		$description_y = $title_y + $author_font_size + 20 +$title_font_adjuster + 14-$body_font_adjuster;
		// Write it
		imagettftext($background, 14-$body_font_adjuster, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		//Line 2
		$description_buffer = substr($description, $description_max_len, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*2] != ' ' && $description_max_len*2 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox(14-$body_font_adjuster, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		$description_y = $title_y + $author_font_size + 20 +$title_font_adjuster + 14-$body_font_adjuster;
		// Write it
		imagettftext($background, 14-$body_font_adjuster, 0, $x, $description_y+14-$body_text_adjuster, $body_text_color, $font_path, $description_buffer);
		
		//Line 3
		$description_buffer = substr($description, $description_max_len*2, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*3] != ' ' && $description_max_len*3 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox(14-$body_font_adjuster, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		$description_y = $title_y + $author_font_size + 20 +$title_font_adjuster + 14-$body_font_adjuster;
		// Write it
		imagettftext($background, 14-$body_font_adjuster, 0, $x, $description_y+2*(14-$body_text_adjuster), $body_text_color, $font_path, $description_buffer);
		
		
		//Line 4
		$description_buffer = substr($description, $description_max_len*3, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*4] != ' ' && $description_max_len*4 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox(14-$body_font_adjuster, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		$description_y = $title_y + $author_font_size + 20 +$title_font_adjuster + 14-$body_font_adjuster;
		// Write it
		imagettftext($background, 14-$body_font_adjuster, 0, $x, $description_y+3*(14-$body_text_adjuster), $body_text_color, $font_path, $description_buffer);
		
	}
	if($width == 1000){

		$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
		$x = 1;
		$y = $bbox[1];
		if($bbox[2] > 280){
			while($bbox[2] > 280){
				$title_font_size-=$x;
				$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
				$x++;
				$y = $bbox[1];
			}
		}
		imagettftext($background, $title_font_size, 0, 10, $title_font_size+10, $main_header_color, $font_path, $title);
		$des_len_adjuster = $bbox[2];
		
		$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
		$x = 1;
		$y = $bbox[1];
		if($bbox[2] > 280){
			while($bbox[2] > 280){
				$author_font_size-=$x;
				$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
				$x++;
				$y = $bbox[1];
			}
		}
		if($author_font_size > 12)
			$author_font_size = 12;
		imagettftext($background, $author_font_size, 0, 10, $title_font_size+$author_font_size+20, $sub_header_color, $font_path, $author);
		
		
		
		$description_max_len = (1-(600/(1000-$des_len_adjuster))+1)*$description_max_len;		
		$x = $des_len_adjuster+40;
		if(strlen($description) > $description_max_len * 4)
			header("Location: ../index.php?error=description&description=" . $_POST['description'] . "&author=" . $_POST['author'] . "&title=" . $_POST['title']);
		
		//Line 1
		$description_buffer = substr($description, 0, $description_max_len);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len] != ' ' && strlen($description) > $description_max_len)
			$description_buffer = $description_buffer . "-";
		
		$description_y = 15;
		// Write it
		imagettftext($background, 14-$body_font_adjuster, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		//Line 2
		$description_buffer = substr($description, $description_max_len, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*2] != ' ' && $description_max_len*2 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$description_y = 30;
		// Write it
		imagettftext($background, 14-$body_font_adjuster, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		//Line 3
		$description_buffer = substr($description, $description_max_len*2, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*3] != ' ' && $description_max_len*3 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$description_y = 45;
		// Write it
		imagettftext($background, 14-$body_font_adjuster, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		
		//Line 4
		$description_buffer = substr($description, $description_max_len*3, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*4] != ' ' && $description_max_len*4 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$description_y = 60;
		// Write it
		imagettftext($background, 14-$body_font_adjuster, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
	}
	imagepng($background);
	imagedestroy($gradient);
?>