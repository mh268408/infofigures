<?php

	if($_GET['size'] != null)
		$size = "size=" . $_GET['size'];
	else
		$size = null;
		
	if($_GET['pallet'] != null)
		$pallet = "pallet=" . $_GET['pallet'];
	else
		$pallet = null;
		
	if($_GET['font'] != null)
		$font = "font=" . $_GET['font'];
	else
		$font = null;
	
	$x=1;
	if(!file_exists("1.png") && file_exists("preview.png")){
		unlink("preview.png");
		header("Location:../index.php?" . $pallet . "&" . $font);
	}
	if(!file_exists("1.png"))
		header("Location:../index.php?" . $pallet . "&" . $font);
	if(file_exists("preview.png"))
		unlink("preview.png");
	$total_height=0;
	
	while(file_exists($x. ".png")){
		list($width, $height) = getimagesize($x . ".png");
		$total_height += $height;
		$x++;
	}
	
	$background=imagecreatetruecolor($width, $total_height);
	$bgColor = imagecolorallocate($background, 0, 0, 0);
	imagefill($background, 0, 0, $bgColor);
	
	$background_width = $width;
	$background_height = $total_height;
	$x = 1;
	$current_height=0;
	while(file_exists($x . ".png")){
		list($width, $height) = getimagesize($x. ".png");
		imagecopy($background, imagecreatefrompng($x.".png"), 0, $current_height, 0, 0, $background_width, $background_height);
		$current_height+=$height;
		$x++;
	}

	imagepng($background, "preview.png");
	
	header("Location:../index.php?preview=true&" . $pallet . "&" . $size . "&" . $font);
		
?>