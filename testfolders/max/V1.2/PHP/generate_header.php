<?php
	//libraries
	require_once("gradient-fill.php"); //needed to make the gradients
	require_once("colors.php"); //pallet and size data

	//variables 
	$pallet = $_POST['pallet'];
	$size = $_POST['size'];
	$title = $_POST['title'];
	$title = str_replace("\'", "'", $title);
	$title = str_replace('\"', '"', $title);
	$author=$_POST['author'];
	$author = str_replace("\'", "'", $author);
	$author = str_replace('\"', '"', $author);
	$description = $_POST['description'];
	$description = str_replace("\'", "'", $description);
	$description = str_replace('\"', '"', $description);
	$layout = $_POST['layout'];
	$font = $_POST['font'];
	$author_font_size = 18;
	$title_font_size = 50;
	$description_font_size = 10;
	$description_max_len = 50;
	
	switch($size){
		case "small": $width = $small;
						break;
		case "medium": $width = $medium;
						break;
		case "large": $width = $large;
						break;
	}

	foreach($pallets_html as $key => $html){
		if($html[0] == $pallet){
			$bgcolor_reference = $pallets_text[$key][0];
			$main_header_color_ref = $pallets_text[$key][1];
			$sub_header_color_ref = $pallets_text[$key][2];
			$body_text_color_ref = $pallets_text[$key][3];
		}
	}
	
	foreach($fonts as $current){
		if($current[2] == $font)
			$font_path = $current[0];
	}
	
	if($font_path == null)
		$font_path = "../fonts/arial.ttf";
	if($bgcolor_reference == null){
		$bgcolor_reference = $black_on_white_text[0];
		$main_header_color_ref = $black_on_white_text[1];
		$sub_header_color_ref = $black_on_white_text[2];
		$body_text_color_ref = $black_on_white_text[3];
	}
	if($width == null || $width == 'n/a')
		$width = "800";
	/*
	switch($pallet){
		case "underground": $bgcolor_reference = $underground_text[0];
							$main_header_color_ref = $underground_text[1];
							$sub_header_color_ref = $underground_text[2];
							$body_text_color_ref = $underground_text[3];
							break;
		case "clownfish": $bgcolor_reference = $clownfish_text[0];
						  $main_header_color_ref = $clownfish_text[1];
						  $sub_header_color_ref = $clownfish_text[2];
						  $body_text_color_ref = $clownfish_text[3];
						  break;
		case "wine": $bgcolor_reference = $wine_text[0];
					 $main_header_color_ref = $wine_text[1];
					 $sub_header_color_ref = $wine_text[2];
					 $body_text_color_ref = $wine_text[3];
					 break;
		case "dribble": $bgcolor_reference = $dribble_text[0];
						$main_header_color_ref = $dribble_text[1];
						$sub_header_color_ref = $dribble_text[2];
						$body_text_color_ref = $dribble_text[3];
						break;
		case "black_on_white": $bgcolor_reference = $black_on_white_text[0];
						$main_header_color_ref = $black_on_white_text[1];
						$sub_header_color_ref = $black_on_white_text[2];
						$body_text_color_ref = $black_on_white_text[3];
						break;
		case "white_on_black": $bgcolor_reference = $white_on_black_text[0];
						$main_header_color_ref = $white_on_black_text[1];
						$sub_header_color_ref = $white_on_black_text[2];
						$body_text_color_ref = $white_on_black_text[3];
						break;
	}
	*/


	$bgcolor_referencergb = hex2rgb($bgcolor_reference);
	$main_header_color_ref_rgb = hex2rgb($main_header_color_ref);
	$sub_header_color_ref_rgb = hex2rgb($sub_header_color_ref);
	$body_text_color_ref_rgb = hex2rgb($body_text_color_ref);


	if($bgcolor_referencergb[0] + 50 > 255)
		$bgcolor_referencergb[0] = 255;
	else
		$bgcolor_referencergb[0]=$bgcolor_referencergb[0]+50;

	if($bgcolor_referencergb[1] + 50 > 255)
		$bgcolor_referencergb[1] = 255;
	else
		$bgcolor_referencergb[1]=$bgcolor_referencergb[1]+50;
			

	if($bgcolor_referencergb[2] + 50 > 255)
		$bgcolor_referencergb[2] = 255;
	else
		$bgcolor_referencergb[2]=$bgcolor_referencergb[2]+50;
			
		
	$color2=rgb2hex($bgcolor_referencergb);


	$gradient_spawn = new gd_gradient_fill($width,100,'vertical',$color2,$bgcolor_reference);
	 
	 

	$bgcolor_referencergb = hex2rgb($bgcolor_reference); 


	//Create background and copy it to the gradient
	$gradient = imagecreatefrompng('gradient.png');

	$background = imagecreate($width, 300);
	
	$backgroundcolor = imagecolorallocate($background, $bgcolor_referencergb[0], $bgcolor_referencergb[1], $bgcolor_referencergb[2]);
	$bgcolor = imagecolorallocate($background, $bgcolor_reference_explode[0], $bgcolor_reference_explode[1], $bgcolor_reference_explode[2]); 
	imagecopymerge($background, $gradient, 0, 0, 0, 0, $width, 100, 100);

	$main_header_color = imagecolorallocate($background, $main_header_color_ref_rgb[0], $main_header_color_ref_rgb[1], $main_header_color_ref_rgb[2]);
	$sub_header_color = imagecolorallocate($background, $sub_header_color_ref_rgb[0], $sub_header_color_ref_rgb[1], $sub_header_color_ref_rgb[2]);
	$body_text_color = imagecolorallocate($background, $body_text_color_ref_rgb[0], $body_text_color_ref_rgb[1], $body_text_color_ref_rgb[2]);

	
	if($layout == 'one'){
		$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
		$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
		if($bboxWidth > 150){
			while($bboxWidth > 150){
				$author_font_size-=1;
				$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		else{
			while($bboxWidth < 150){
				$author_font_size+=1;
				if((abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1) > 20)
					break;
				$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		$authorHeight=abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1;
		imagettftext($background, $author_font_size, 0, $width-160, $authorHeight+10, $sub_header_color, $font_path, $author);
		
		
		$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
		$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
		if($bboxWidth > $width-50){
			while($bboxWidth > $width-50){
				$title_font_size-=1;
				$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		else{
			while($bboxWidth < $width-50){
				$title_font_size+=1;
				if((abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1) > 100)
					break;
				$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		$titleHeight=abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1;
		imagettftext($background, $title_font_size, 0, 10, $authorHeight+$titleHeight+20, $main_header_color, $font_path, $title);
		
		
		$bbox = imagettfbbox($description_font_size, 0, $font_path, substr($description, 0, $description_max_len));
		$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
		if($bboxWidth > $width-30){
			while($bboxWidth > $width-30){
				$description_max_len-=1;
				$bbox = imagettfbbox($description_font_size, 0, $font_path, substr($description, 0, $description_max_len));
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		else{
			while($bboxWidth < $width-30){
				if(strlen($description) < $description_max_len)
				 break;
				else{
					$description_max_len+=1;
					$bbox = imagettfbbox($description_font_size, 0, $font_path, substr($description, 0, $description_max_len));
					$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
				}
			}
		}
		
		
		if(strlen($description) <= $description_max_len){
			imagettftext($background, $description_font_size, 0, 10, $authorHeight+$titleHeight+30+$description_font_size, $body_text_color, $font_path, $description); $lines = 1;
		}
		else{
			//line 1
			$description_buffer = substr($description, 0, $description_max_len); $lines = 1;
			if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len] != ' ' && strlen($description) > $description_max_len)
				$description_buffer = $description_buffer . "-";
			imagettftext($background, $description_font_size, 0, 10, $authorHeight+$titleHeight+30+$description_font_size, $body_text_color, $font_path, $description_buffer);

			//Line 2
			$description_buffer = substr($description, $description_max_len, $description_max_len);
			if(strlen($description_buffer) != 0) $lines=2;
			if($description_buffer[0] == ' ')
				$description_buffer = substr($description_buffer, 1, $description_max_len-1);
			if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*2] != ' ' && $description_max_len*2 < strlen($description))
				$description_buffer = $description_buffer . "-";
			imagettftext($background, $description_font_size, 0, 10, $authorHeight+$titleHeight+40+$description_font_size*2, $body_text_color, $font_path, $description_buffer);

			//Line 3
			$description_buffer = substr($description, $description_max_len*2, $description_max_len);
			if(strlen($description_buffer) != 0) $lines=3;
			if($description_buffer[0] == ' ')
				$description_buffer = substr($description_buffer, 1, $description_max_len-1);
			if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*3] != ' ' && $description_max_len*3 < strlen($description))
				$description_buffer = $description_buffer . "-";
			imagettftext($background, $description_font_size, 0, 10, $authorHeight+$titleHeight+50+$description_font_size*3, $body_text_color, $font_path, $description_buffer);

			//Line 4
			$description_buffer = substr($description, $description_max_len*3, $description_max_len);
			if(strlen($description_buffer) != 0) $lines=4;
			if($description_buffer[0] == ' ')
				$description_buffer = substr($description_buffer, 1, $description_max_len-1);
			imagettftext($background, $description_font_size, 0, 10, $authorHeight+$titleHeight+60+$description_font_size*4, $body_text_color, $font_path, $description_buffer);
		}
		
		$final=imagecreatetruecolor($width, $authorHeight+$titleHeight+20+(10*$lines)+$description_font_size*$lines+15);
		imagecopyresampled($final, $background, 0, 0, 0, 0, $width, 300, $width, 300); 
		
		
		
	}
	else if ($layout == 'two'){
		
		$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
		$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
		if($bboxWidth > $width-50){
			while($bboxWidth > $width-50){
				$title_font_size-=1;
				$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		else{
			while($bboxWidth < $width-50){
				$title_font_size+=1;
				if((abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1) > 100)
					break;
				$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		$titleHeight=abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1;
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;

		// Write it
		imagettftext($background, $title_font_size, 0, $x, $titleHeight+10, $main_header_color, $font_path, $title);
		
		$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
		$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
		if($bboxWidth > $width*0.75){
			while($bboxWidth > $width*0.75){
				$author_font_size-=1;
				$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		else{
			while($bboxWidth < $width*0.75){
				$author_font_size+=1;
				if((abs(min(array($bbox[1],$bbox[3],$bbox[5],$bbox[7]))) - 1) > 25)
					break;
				$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		$authorHeight=abs(min(array($bbox[1],$bbox[3],$bbox[5],$bbox[7]))) - 1;
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		imageline($background, $x+10, $authorHeight+$titleHeight+25, $x+$bboxWidth-10, $authorHeight+$titleHeight+25, $sub_header_color);
		
		// Write it
		imagettftext($background, $author_font_size, 0, $x, $titleHeight+$authorHeight+20, $sub_header_color, $font_path, $author);
		
		
		$bbox = imagettfbbox($description_font_size, 0, $font_path, substr($description, 0, $description_max_len));
		$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
		if($bboxWidth > $width-70){
			while($bboxWidth > $width-70){
				$description_max_len-=1;
				$bbox = imagettfbbox($description_font_size, 0, $font_path, substr($description, 0, $description_max_len));
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		else{
			while($bboxWidth < $width-70){
				if(strlen($description) < $description_max_len)
				 break;
				else{
					$description_max_len+=1;
					$bbox = imagettfbbox($description_font_size, 0, $font_path, substr($description, 0, $description_max_len));
					$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
				}
			}
		}
		
		//Line 1
		$description_buffer = substr($description, 0, $description_max_len);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len] != ' ' && strlen($description) > $description_max_len)
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox($description_font_size, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		$description_y = $titleHeight+$authorHeight+$description_font_size+30;
		// Write it
		imagettftext($background, $description_font_size, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		//Line 2
		$description_buffer = substr($description, $description_max_len, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*2] != ' ' && $description_max_len*2 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox($description_font_size, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		if(strlen($description_buffer) != 0) $description_y+=$description_font_size+10;
		// Write it
		imagettftext($background, $description_font_size, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		//Line 3
		$description_buffer = substr($description, $description_max_len*2, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*3] != ' ' && $description_max_len*3 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox($description_font_size, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		if(strlen($description_buffer) != 0) $description_y+=$description_font_size+10;
		// Write it
		imagettftext($background, $description_font_size, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		
		//Line 4
		$description_buffer = substr($description, $description_max_len*3, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*4] != ' ' && $description_max_len*4 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox($description_font_size, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $bbox[0] + (imagesx($background) / 2) - ($bbox[4] / 2) - 5;
		if(strlen($description_buffer) != 0) $description_y+=$description_font_size+10;
		// Write it
		imagettftext($background, $description_font_size, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		$final=imagecreatetruecolor($width, $description_y+15);
		imagecopyresampled($final, $background, 0, 0, 0, 0, $width, 300, $width, 300); 
	}
	else{
		$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
		$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
		if($bboxWidth > $width*0.35){
			while($bboxWidth > $width*0.35){
				$title_font_size-=1;
				$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		else{
			while($bboxWidth < $width*0.35){
				$title_font_size+=1;
				if((abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1) > 100)
					break;
				$bbox = imagettfbbox($title_font_size, 0, $font_path, $title);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		$titleHeight=abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1;
		$titleWidth = $bboxWidth + 20;
		// Write it
		imagettftext($background, $title_font_size, 0, 10, $titleHeight+10, $main_header_color, $font_path, $title);
		
		$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
		$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
		if($bboxWidth > $width*0.2){
			while($bboxWidth > $width*0.2){
				$author_font_size-=1;
				$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		else{
			while($bboxWidth < $width*0.2){
				$author_font_size+=1;
				if((abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1) > 25)
					break;
				$bbox = imagettfbbox($author_font_size, 0, $font_path, $author);
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		$authorHeight=abs(min(array($bbox[2],$bbox[3],$bbox[5],$bbox[7]))) - 1;

		// Write it
		imagettftext($background, $author_font_size, 0, 10, $titleHeight+$authorHeight+20, $sub_header_color, $font_path, $author);
		
		
		$bbox = imagettfbbox($description_font_size, 0, $font_path, substr($description, 0, $description_max_len));
		$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
		if($bboxWidth > $width*0.60){
			while($bboxWidth > $width*0.60){
				$description_max_len-=1;
				$bbox = imagettfbbox($description_font_size, 0, $font_path, substr($description, 0, $description_max_len));
				$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
			}
		}
		else{
			while($bboxWidth < $width*0.60){
				if(strlen($description) < $description_max_len)
				 break;
				else{
					$description_max_len+=1;
					$bbox = imagettfbbox($description_font_size, 0, $font_path, substr($description, 0, $description_max_len));
					$bboxWidth= max(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]))-min(array($bbox[0],$bbox[2],$bbox[4],$bbox[6]));
				}
			}
		}
		
		//Line 1
		$description_buffer = substr($description, 0, $description_max_len);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len] != ' ' && strlen($description) > $description_max_len)
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox($description_font_size, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $titleWidth;
		$description_y = $description_font_size + 10;
		// Write it
		imagettftext($background, $description_font_size, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		//Line 2
		$description_buffer = substr($description, $description_max_len, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*2] != ' ' && $description_max_len*2 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox($description_font_size, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $titleWidth;
		if(strlen($description_buffer) != 0) $description_y+=$description_font_size+10;
		// Write it
		imagettftext($background, $description_font_size, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		//Line 3
		$description_buffer = substr($description, $description_max_len*2, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*3] != ' ' && $description_max_len*3 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox($description_font_size, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $titleWidth;
		if(strlen($description_buffer) != 0) $description_y+=$description_font_size+10;
		// Write it
		imagettftext($background, $description_font_size, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		
		//Line 4
		$description_buffer = substr($description, $description_max_len*3, $description_max_len);
		if($description_buffer[0] == ' ')
			$description_buffer = substr($description_buffer, 1, $description_max_len-1);
		if($description_buffer[$description_max_len-1] != ' ' && $description[$description_max_len*4] != ' ' && $description_max_len*4 < strlen($description))
			$description_buffer = $description_buffer . "-";
		$bbox = imagettfbbox($description_font_size, 0, $font_path, $description_buffer);
		// This is our cordinates for X and Y
		$x = $titleWidth;
		if(strlen($description_buffer) != 0) $description_y+=$description_font_size+10;
		// Write it
		imagettftext($background, $description_font_size, 0, $x, $description_y, $body_text_color, $font_path, $description_buffer);
		
		$finalHeight = max(array($titleHeight+$authorHeight+35, $description_y+15));
		
		$final=imagecreatetruecolor($width, $finalHeight);
		imagecopyresampled($final, $background, 0, 0, 0, 0, $width, 300, $width, 300); 
	}

	$x = 0;
	do{
		$x++;
	}while(file_exists($x.".png"));
	imagepng($final, $x.".png");
	header("Location: ../index.php?preview=true&size=" . $_POST['size'] . "&pallet=" . $_POST['pallet'] . "&layout=" . $_POST['layout'] . "&font=" . $_POST['font']);
	imagedestroy($gradient);
?>