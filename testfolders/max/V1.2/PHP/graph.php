<?php
require_once("colors.php");
require_once("gradient-fill.php");

$pallet = "underground";
$width = $small/2;
$bg_color_ref;
$colors_ref;
$colorsrgb = array(0,1,2,3);
$colors = array(0,1,2,3);
$x_position = 500;
$y_position = 500;

switch($pallet){
	case "underground": $bgcolor_reference = $underground_graphics[0];
						$colors_ref = array($underground_graphics[1], $underground_graphics[2], $underground_graphics[3], $underground_graphics[4]);
						break;
	case "clownfish": $bgcolor_reference = $clownfish_graphics[0];
					  $colors_ref = array($clownfish_graphics[1], $clownfish_graphics[2], $clownfish_graphics[3], $clownfish_graphics[4]);
					  break;
	case "wine": $bgcolor_reference = $wine_graphics[0];
				 $colors_ref = array($wine_graphics[1], $wine_graphics[2], $wine_graphics[3], $wine_graphics[4]);
				 break;
	case "dribble": $bgcolor_reference = $dribble_graphics[0];
					$colors_ref = array($dribble_graphics[1], $dribble_graphics[2], $dribble_graphics[3], $dribble_graphics[4]);					
					break;
}
					
$image = imagecreatetruecolor($width+100, $width+100);

$bgcolor_referencergb = hex2rgb($bgcolor_reference);
$colorsrgb[0] = hex2rgb($colors_ref[0]);
$colorsrgb[1] = hex2rgb($colors_ref[1]);
$colorsrgb[2] = hex2rgb($colors_ref[2]);
$colorsrgb[3] = hex2rgb($colors_ref[3]);

$backgroundcolor = imagecolorallocate($image, $bgcolor_referencergb[0], $bgcolor_referencergb[1], $bgcolor_referencergb[2]);
$colors[0] = imagecolorallocate($image, $colorsrgb[0][0], $colorsrgb[0][1], $colorsrgb[0][2]);
$colors[1] = imagecolorallocate($image, $colorsrgb[1][0], $colorsrgb[1][1], $colorsrgb[1][2]);
$colors[2] = imagecolorallocate($image, $colorsrgb[2][0], $colorsrgb[2][1], $colorsrgb[2][2]);
$colors[3] = imagecolorallocate($image, $colorsrgb[3][0], $colorsrgb[3][1], $colorsrgb[3][2]);

imagefilledarc ($image , $x_position , $y_position , $width , $width , 0 , 300 , $colors[0] , IMG_ARC_PIE);
imagefilledarc ($image, $x_position, $y_position, $width-50, $width-50, 0, 300, $colors[1], IMG_ARC_PIE);
imagefilledarc ($image, $x_position, $y_position, $width-100, $width-100, 0, 300, $colors[2], IMG_ARC_PIE);
imagefilledarc ($image, $x_position, $y_position, $width-150, $width-150, 0, 300, $colors[3], IMG_ARC_PIE);

header('Content-type: image/png');
imagepng($image);
imagedestroy($image);

?>