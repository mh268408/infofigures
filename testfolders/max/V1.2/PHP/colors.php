<?php

	//Color one: background color
	//Color two: Main headers
	//Color three: Sub headers
	//Color four: Body text
	
	
	$small="600";
	
	$medium="800";
	
	$large="1000";
	
	$underground_text=array("#E3CBAC", "#324654", "#9C9985", "#788880");
	$underground_graphics=array("#C46D3B", "#9C9985", "#788880", "#324654");
	$underground_html=array("underground", "Underground", array("E3CBAC", "9C9985", "C46D3B"));
	
	$clownfish_text=array("#F25C05", "#E5EFFA", "#A63D00", "#FFCEB1");
	$clownfish_graphics=array("#E5EFFA", "#F28705", "#93A605", "#ffb489");
	$clownfish_html=array("clownfish", "Clownfish", array("3E5916", "93A605", "F28705", "F25C05"));
	
	$wine_text=array("#DBDAB7", "#47282C", "#8C8468", "#604649");
	$wine_graphics=array("#C9B37F", "#C4C49C", "#8C8468", "#47282C");
	$wine_html=array("wine", "Wine", array("47282C", "8C8468", "C9B37F", "D8DAB7"));
	
	$dribble_text=array("#70B7BA", "#F1433F", "#3D4C53", "#604649");
	$dribble_graphics=array("#E7E1D4", "#F1433F", "#3D4C53", "#FFFFFF");
	$dribble_html=array("dribble", "Dribble", array("3D4C53", "70B7BA", "F1433F", "E7E1D4"));
	
	$black_on_white_text=array("#FFFFFF", "#000000", "#000000", "#000000");
	$black_on_white_graphics=array("#000000", "#000000", "#000000", "#000000");
	$black_on_white_html=array("black_on_white", "Black on White", array("FFFFFF", "000000"));
	
	$white_on_black_text=array("#000000", "#FFFFFF", "#FFFFFF", "#FFFFFF");
	$white_on_black_graphics=array("#000000", "#000000", "#000000", "#000000");
	$white_on_black_html=array("white_on_black", "White on Black", array("000000", "FFFFFF"));
	
	$swampy_text=array("#303336", "#FFFFFF", "#27bdb5", "#5b8244");
	$swampy_graphics=array("#5b8244", "#1d726d", "#27bdb5", "#a4b2bf");
	$swampy_html=array("swampy", "Swampy", array("5b8244", "303336", "1d726d", "27bdb5"));
	
	$marvin_text=array("#f0e69b", "#363036", "#bb7855", "#807070");
	$marvin_graphics=array("#ffdf14", "#cb9995", "#bb7855", "#807070");
	$marvin_html=array("marvin", "Marvin", array("f0e69b", "cb9995", "bb7855", "807070"));
	
	$spring_text=array("#14696b", "#FFFFFF", "#87c356", "#bdd753");
	$spring_graphics=array("#32d8db", "#509754", "#87c356", "#bdd753");
	$spring_html=array("spring", "Spring", array("14696b", "509754", "87c356", "bdd753"));
	
	$freeroad=array("../fonts/Freeroad.ttf", "Free Road", "freeroad");
	$arial=array("../fonts/arial.ttf", "Arial", "arial");
	$fonts=array($freeroad, $arial);
	
	$pallets_graphics=array($underground_graphics, $clownfish_graphics, $wine_graphics, $dribble_graphics, $marvin_graphics, $spring_graphics, $swampy_graphics, $black_on_white_graphics, $white_on_black_graphics);
	$pallets_html=array($underground_html, $clownfish_html, $wine_html, $dribble_html, $marvin_html, $spring_html, $swampy_html, $black_on_white_html, $white_on_black_html);
	$pallets_text=array($underground_text, $clownfish_text, $wine_text, $dribble_text, $marvin_text, $spring_text, $swampy_text, $black_on_white_text, $white_on_black_text);
	
	?>