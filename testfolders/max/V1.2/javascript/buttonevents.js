$("#small").click(function () {
	$(this).disableSelection();
	if(size_select == "#small"){
		$("#small").attr('style', 'color: white; background-color: #666666');
		$(".header_panel_options_content #size").attr('value', 'n/a');
		$(".footer_panel_options_content #size").attr('value', 'n/a');
		$("#sizebar").attr('value', 'n/a');
		size_select = "nothing";
	}
	else{
		$("#small").attr('style', 'color: #70d3d3; background-color: #3d3d3d');
		$(size_select).attr('style', 'color: white; background-color: #666666');
		$(".header_panel_options_content #size").attr('value', 'small');
		$(".footer_panel_options_content #size").attr('value', 'small');
		$("#sizebar").attr('value', 'small');
		size_select = "#small";
	}
});
$("#medium").click(function () {
	$(this).disableSelection();
	if(size_select == "#medium"){
		$("#medium").attr('style', 'color: white; background-color: #666666');
		$(".header_panel_options_content #size").attr('value', 'n/a');
		$(".footer_panel_options_content #size").attr('value', 'n/a');
		$("#sizebar").attr('value', 'n/a');
		size_select = "nothing";
	}
	else{
		$("#medium").attr('style', 'color: #70d3d3; background-color: #3d3d3d');
		$(size_select).attr('style', 'color: white; background-color: #666666');
		$(".header_panel_options_content #size").attr('value', 'medium');
		$(".footer_panel_options_content #size").attr('value', 'medium');
		$("#sizebar").attr('value', 'medium');
		size_select = "#medium";
	}
});
$("#large").click(function () {
	$(this).disableSelection();
	if(size_select == "#large"){
		$("#large").attr('style', 'color: white; background-color: #666666');
		$(".header_panel_options_content #size").attr('value', 'n/a');
		$(".footer_panel_options_content #size").attr('value', 'n/a');
		$("#sizebar").attr('value', 'n/a');
		size_select = "nothing";
	}
	else{
		$("#large").attr('style', 'color: #70d3d3; background-color: #3d3d3d');
		$(size_select).attr('style', 'color: white; background-color: #666666');
		$(".header_panel_options_content #size").attr('value', 'large');
		$(".footer_panel_options_content #size").attr('value', 'large');
		$("#sizebar").attr('value', 'large');
		size_select = "#large";
	}
});

/*
$('#arial').click(function(){
	$(this).disableSelection();
	if(font_select == '#arial'){
		$(this).attr('style', 'background-color: #FFFFFF; color: #000000;  padding-left: 5px;');
		$('#font').attr('value', 'n/a');
		font_select = 'nothing';
	}
	else{
		$(this).attr('style', 'color: #70d3d3; background-color: #e0e0e0; padding-left: 20px;');
		$(font_select).attr('style', 'color: #000000; background-color: #FFFFFF; padding-left: 5px;');
		$('#font').attr('value', 'arial');
		font_select = '#arial';
	}
});
*/