<html>
<head>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<link rel="Stylesheet" type="text/css" href="stylesheets/style.css" />
	<link rel="Stylesheet" type="text/css" href="stylesheets/font_style.css" />
	<link rel="Stylesheet" type="text/css" href="stylesheets/pallet_style.css" />
	<link rel="Stylesheet" type="text/css" href="stylesheets/size_select_style.css" />
	<link rel="Stylesheet" type="text/css" href="stylesheets/footer_panel_style.css" />
	<link rel="Stylesheet" type="text/css" href="stylesheets/header_panel_style.css" />
	<link rel="Stylesheet" type="text/css" href="stylesheets/bar_panel_style.css" />
	
			<script src="javascript/jquery-1.7.2.min.js" type="text/javascript"></script>
			<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
			<script>
			var selected = null;
			var font_clicked=0;
			var currentset=false;
			var currentpastset=false;
			var currentchange=1;
			var current=1;
			var num_added = 0;
			var pallet_clicked=0;
			var preview_shown = 0;
			var size_select = null;
			var font_select = 'nothing';
			var header_panel_shown=0;
			 $.fn.extend({ 
                disableSelection: function() { 
                    this.each(function() { 
                        if (typeof this.onselectstart != 'undefined') {
                            this.onselectstart = function() { return false; };
                        } else if (typeof this.style.MozUserSelect != 'undefined') {
                            this.style.MozUserSelect = 'none';
                        } else {
                            this.onmousedown = function() { return false; };
                        }
                    }); 
                } 
            });
	
		</script>
	<title>Infographic V1.2</title>
</head>

<body>
	<div class="background_fill"></div>

	
	<?php require_once('panel_options/header.html'); ?>
	<?php require_once('panel_options/footer.html'); ?>
	<?php require_once('panel_options/bar.html'); ?>
	<?php require_once('panel_options/javascript.html'); ?>

	
	
	<div class="header">
		<img src="images/logo.jpg">
	</div>
	
	<div class="preview"  >
		<center class="preview_header">Preview</center>
		<?php if(!file_exists("php/preview.png"))
			echo ("<center><h2> No panels added!</h2></center>");
			else
			echo('<img class="p_image" src="php/preview.png?x=<?=' . time() . '?">');
		?>
		<center>
		<INPUT TYPE="button" class="submit" style="margin-top: 10px;" onClick="window.location.href='php/generate_preview.php<?php 
			$variables = "?";
			if($_GET['size'] != null)
				$variables = $variables."size=".$_GET['size'] . "&";
			if($_GET['pallet'] != null)
				$variables = $variables."pallet=" . $_GET['pallet'] . "&";
			if($_GET['font'] != null)
				$variables = $variables."font=" . $_GET['font'] . "&";
			echo($variables . "'\"");
			?> VALUE="Refresh">
		<INPUT TYPE="button" class="submit" style="margin-top: 10px;" onClick="window.location.href='PHP/preview.png'" VALUE="Download">
		</center>
	</div>
	
	<div class="delete">
	<center class="preview_header">Delete Panels</center>
	<?php
			$x=1;
			$variables = "?";
			if($_GET['size'] != null)
				$variables = $variables."size=".$_GET['size'] . "&";
			if($_GET['pallet'] != null)
				$variables = $variables."pallet=" . $_GET['pallet'] . "&";
			if($_GET['font'] != null)
				$variables = $variables."font=" . $_GET['font'] . "&";
			while(file_exists("PHP/".$x.".png")){
				echo ("\t\t<img class='p_image' src='php/" . $x . ".png'>\n");
				echo ("\t\t<br><center><img class='remove_preview' id='close_" . $x . "' src='images/close.png'></center> \n");
				echo ("<script>");
				echo("$('#close_" . $x . "').click(function(){
						window.location.href='php/delete.php" . $variables . "name=" . $x . "';
						
						});");
				echo("</script>");
				$x++;
			}
		if(!file_exists("php/preview.png"))
			echo ("<center><h2> No panels added!</h2></center>");
		?>
	</div>
	
	<div class="main_options">
		<?php if($_GET['size'] != null) echo("<div class='hide'>"); ?>
	
		<div class="size_box" id="small" name="small">
			<h2 style="font-size: 15px;" class="size_select">Small</h2>
		</div>
		<div class="size_box" id="medium" name="medium">
				<h2 class="size_select">Medium</h2>
		</div>
		<div class="size_box" id="large" name="large">
				<h2 style="font-size:25px;" class="size_select">Large</h2>
		</div>
		<?php if($_GET['size'] != null) echo("</div>"); ?>
		<?php if($_GET['size'] != null){
		echo("<div style='float: right'><input type='button' style='margin-top: 5px; margin-right: 5px; width: auto; height: 35px;' class='submit' onClick='window.location.href=\"php/delete.php?deleteall=true\"'value='Reset!'></div>");
		}
		?>
		
		<div class="pallet_button" id="pallet_button" name="pallet_button">
			<h2 style="font-size:14px;" name="pallet_header" id="pallet_header" class="size_select">Pallet</h2>
		</div>
		
		<div class="font_button" id="font_button" name="font_button">
				<h2 style="font-size:14px;" name="font_header" id="font_header" class="size_select">Font</h2>
		</div>
		
		<script>
		
			 $("#pallet_button").click(function () {
				if(font_clicked == 1){
					$(".font_selector").slideToggle("fast");
					$("#font_button").toggleClass('font_button_clicked');
					font_clicked=0;
				}
				if(pallet_clicked==0)
					pallet_clicked=1;
				else
					pallet_clicked=0;
				$(this).disableSelection();
				$(this).toggleClass('pallet_button_clicked');
				$(".pallet_selector").slideToggle("fast");
			});
			 $("#font_button").click(function () {
				 if(pallet_clicked == 1){
					$(".pallet_selector").slideToggle("fast");
					$("#pallet_button").toggleClass('pallet_button_clicked');
					pallet_clicked=0;
				}
				if(font_clicked==0)
					font_clicked=1;
				else
					font_clicked=0;
				$(this).disableSelection();
				$(this).toggleClass('font_button_clicked');
				$(".font_selector").slideToggle("fast");
			});
			
		</script>
	
	</div>
	
		<?php if($_GET['pallet'] != null) echo("<script> selected='#" . $_GET['pallet'] . "';</script>"); ?>
		<?php if($_GET['font'] != null) echo("<script> font_select='#" . $_GET['font'] . "';</script>"); ?>
	
	
	<div class="panel_selector">
			<div class="font_selector">
<?php 
			require_once("PHP/colors.php"); //pallet and size data
			
			foreach($fonts as $current){
				if($_GET['font'] == $current[2])
						echo ("\t\t\t<div class='font' id='" . $current[2] .  "' style='padding-left: 20px; background-color: e0e0e0; color: 70d3d3; '>\n");
					else
						echo("\t\t\t<div class='font' id='" . $current[2] . "' >\n");
				echo ("\t\t\t\t<h2>" . $current[1] . "</h2>\n");
				echo ("\t\t\t\t<img style='float: right;' src='images/" . $current[2] . "_sample.png'>\n");
				echo("\t\t\t</div>\n");
			}
			?>
		</div>
				
		<div class="pallet_selector">
<?php
				require_once("PHP/colors.php"); 
				foreach($pallets_html as $current){
					if($_GET['pallet'] == $current[0])
						echo ("\t\t\t<div class='pallet' id='" . $current[0] .  "' style='background-color: 3d3d3d; color: 70d3d3; '>\n");
					else
						echo("\t\t\t<div class='pallet' id='" . $current[0] . "' >\n");
					echo("\t\t\t\t<h2>" . $current[1] . "</h2>\n");
					foreach($current[2] as $colors){
						echo ("\t\t\t\t<div class='pallet_color' style='background-color: #" . $colors . ";'> </div>\n");
					}
					echo ("\t\t\t</div>\n");
				}
				?>
			
				<script>
<?php
				require_once("PHP/colors.php"); 
				foreach($pallets_html as $current){
					echo("\t\t\t\t\t$('#" . $current[0] . "').click(function () {\n");
						echo ("\t\t\t\t\t\t$(this).disableSelection();\n");
						echo ("\t\t\t\t\t\tif(selected == '#" . $current[0] . "'){\n");
							echo("\t\t\t\t\t\t\t$(this).attr('style', 'color: white; background-color: #666666');\n");
							echo("$('.footer_panel_options_content #pallet').attr('value', 'n/a');\n");
							echo("$('.header_panel_options_content #pallet').attr('value', 'n/a');\n");
							echo("$('#palletbar').attr('value', 'n/a');\n");
							echo ("selected = 'nothing';\n");
						echo("\t\t\t\t\t\t}\n");
						echo("\t\t\t\t\t\t\telse{");
							echo("\t\t\t\t\t\t\t$(this).attr('style', 'color: #70d3d3; background-color: #3d3d3d');\n");
							echo("\t\t\t\t\t\t\t$(selected).attr('style', 'color: white; background-color: #666666');\n");
							echo("\t\t\t\t\t\t\t$('.header_panel_options_content #pallet').attr('value', '" . $current[0] . "');\n");
							echo("\t\t\t\t\t\t\t$('.footer_panel_options_content #pallet').attr('value', '" . $current[0] . "');\n");
							echo("\t\t\t\t\t\t\t$('#palletbar').attr('value', '" . $current[0] . "');\n");
							echo("\t\t\t\t\t\t\tselected = '#" . $current[0] . "';\n");
						echo("\t\t\t\t\t\t}\n");
					echo("\t\t\t\t\t});\n");
				}
				foreach($fonts as $current){
				echo("\t\t\t\t\t$('#" . $current[2] . "').click(function () {\n");
						echo ("\t\t\t\t\t\t\t$(this).disableSelection();\n");
						echo ("\t\t\t\t\t\t\tif(font_select == '#" . $current[2] . "'){\n");
						echo ("\t\t\t\t\t\t\t$(this).attr('style', 'background-color: #FFFFFF; color: #000000; padding-left: 5px;');\n");
						echo ("\t\t\t\t\t\t\t$('.footer_panel_options_content #font').attr('value', 'n/a');\n");
						echo ("\t\t\t\t\t\t\t$('.header_panel_options_content #font').attr('value', 'n/a');\n");
						echo ("\t\t\t\t\t\t\t$('#fontbar').attr('value', 'n/a');\n");
						echo ("\t\t\t\t\t\t\tfont_select = 'nothing';\n");
					echo("\t\t\t\t\t\t}\n");
					echo("\t\t\t\t\t\t\telse{");
						echo ("\t\t\t\t\t\t\t$(this).attr('style', 'color: #70d3d3; background-color: #e0e0e0; padding-left: 20px;');\n");
						echo ("\t\t\t\t\t\t\t$(font_select).attr('style', 'color: #000000; background-color: #FFFFFF;  padding-left: 5px;');\n");
						echo ("\t\t\t\t\t\t\t$('.header_panel_options_content #font').attr('value', '" . $current[2] . "');\n");
						echo ("\t\t\t\t\t\t\t$('#fontbar').attr('value', '" . $current[2] . "');\n");
						echo ("\t\t\t\t\t\t\t$('.footer_panel_options_content #font').attr('value', '" . $current[2] . "');\n");
						echo ("\t\t\t\t\t\t\tfont_select = '#" . $current[2] . "';\n");
					echo("\t\t\t\t\t\t}\n");
				echo("\t\t\t\t\t});\n");
				}
				?>
				</script>
				
				<script src="javascript/buttonevents.js" type="text/javascript"></script>
			
		</div>
		
	
		<img src="images/show_preview.png" class="show_preview">
		<img src="images/show_delete.png" class="show_delete">
		<div class="panel" id="header_panel_click" name="header_panel_click">
			<center><h2>Main Header</h2>
			<img src='images/header_medium_sample.png'>
			</center>
		</div>
		<div class="panel" id="footer_panel_click" name="footer_panel_click">
			<center><h2>Footer</h2>
				<img src="images/header_medium_sample.png">
			</center>
		</div>
		<div class="panel" id="bar_panel_click" name="bar_panel_click">
			<center><h2>Bar Graph</h2>
				<img src="images/header_medium_sample.png">
			</center>
		</div>


	</div>
	<script>
	var delete_shown = 0;
		<?php if ($_GET['preview'] != null) 
		echo ('
		$(document).ready(function() {
			$(".main_options").animate({width: "70%"}, "fast");
				$(".panel_selector").animate({width: "70%"}, "fast");
				$(".preview").animate({width: "29.9%"}, "slow");
				$(".preview").fadeIn("slow");
				preview_shown = 1;
		});
		');
		?>
		$(".show_preview").click(function(){
			if(delete_shown == 1){
				$('.delete').fadeOut("slow");
				delete_shown = 0;
			}
			if(preview_shown == 0){
				$(".main_options").animate({width: '70%'}, "fast");
				$(".panel_selector").animate({width: '70%'}, "fast");
				$(".preview").animate({width: '29.9%'}, "slow");
				$('.preview').fadeIn("slow");
				preview_shown = 1;
			}
			else{
				$('.preview').fadeOut("fast");
				$(".main_options").delay(600).animate({width: '100%'}, "slow");
				$(".panel_selector").delay(600).animate({width: '100%'}, "slow");
				preview_shown=0;
			}
		});
		$(".show_delete").click(function(){
			if(preview_shown == 1){
				$('.preview').fadeOut("slow");
				preview_shown=0;
			}
			if(delete_shown == 0){
				$(".main_options").animate({width: '70%'}, "fast");
				$(".panel_selector").animate({width: '70%'}, "fast");
				$(".delete").animate({width: '29.9%'}, "slow");
				$('.delete').fadeIn("slow");
				delete_shown = 1;
			}
			else if(delete_shown == 1){
				$('.delete').fadeOut("slow");
				$(".main_options").delay(600).animate({width: '100%'}, "slow");
				$(".panel_selector").delay(600).animate({width: '100%'}, "slow");
				delete_shown = 0;
			}
		});
		
		$("#header_panel_click").click(function(){
			if(header_panel_shown==0){
				$(".header_panel_options").fadeIn("fast");
				$(".background_fill").fadeIn("fast");
				header_panel_shown=1;
			}
			});
		$(".close_header_options_panel").click(function(){
				$(".header_panel_options").fadeOut("fast");
				$(".background_fill").fadeOut("fast");
				header_panel_shown=0;
			});
		$("#footer_panel_click").click(function(){
			if(header_panel_shown==0){
				$(".footer_panel_options").fadeIn("fast");
				$(".background_fill").fadeIn("fast");
				header_panel_shown=1;
			}
			});
		$(".close_footer_options_panel").click(function(){
				$(".footer_panel_options").fadeOut("fast");
				$(".background_fill").fadeOut("fast");
				header_panel_shown=0;
			});
		$("#bar_panel_click").click(function(){
			if(header_panel_shown==0){
				$(".bar_panel_options").fadeIn("fast");
				$(".background_fill").fadeIn("fast");
				header_panel_shown=1;
			}
			});
		$(".close_bar_options_panel").click(function(){
				$(".bar_panel_options").fadeOut("fast");
				$(".background_fill").fadeOut("fast");
				header_panel_shown=0;
			});
			
	</script>
</body>
</html>