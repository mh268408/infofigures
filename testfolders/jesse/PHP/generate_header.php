<?php
//libraries
require_once("gradient-fill.php"); //needed to make the gradients
require_once("colors.php"); //pallet and size data

//variables 
$pallet = $_POST['pallet'];
$size = $_POST['size'];
$title = $_POST['title'];
$author = "By: " . $_POST['author'];
$description = $_POST['description'];
$description_max_len = 70;
if(strlen($description) > $description_max_len * 4)
	header("Location: ../");
if(strlen($description) > $description_max_len*2)
	$description_array = array(substr($description, 0, $description_max_len), substr($description, $description_max_len, $description_max_len), substr($description, $description_max_len*2, $description_max_len), substr($description, $description_max_len*3, $description_max_len));
else if(strlen($description) > $description_max_len*2)
	$description_array = array(substr($description, 0, $description_max_len), substr($description, $description_max_len, $description_max_len), substr($description, $description_max_len*2, $description_max_len));
else if(strlen($description) > $description_max_len)
	$description_array = array(substr($description, 0, $description_max_len), substr($description, $description_max_len, strlen($description)));

$author_font_adjuster = 5;
$title_font_adjuster = 10;
$max_author_font = 15;
$max_title_font = 45;
$body_font_adjuster = 4;
$width;
$bgcolor_reference;
$main_header_color_ref;
$sub_header_color_ref;
$body_text_color_ref;
$font_path = "../fonts/Freeroad.ttf";
$color2;
switch($size){
	case "small": $width = $small;
					break;
	case "medium": $width = $medium;
					break;
	case "large": $width = $large;
					break;
}

switch($pallet){
	case "underground": $bgcolor_reference = $underground_text[0];
						$main_header_color_ref = $underground_text[1];
						$sub_header_color_ref = $underground_text[2];
						$body_text_color_ref = $underground_text[3];
						break;
	case "clownfish": $bgcolor_reference = $clownfish_text[0];
					  $main_header_color_ref = $clownfish_text[1];
					  $sub_header_color_ref = $clownfish_text[2];
					  $body_text_color_ref = $clownfish_text[3];
					  break;
	case "wine": $bgcolor_reference = $wine_text[0];
				 $main_header_color_ref = $wine_text[1];
				 $sub_header_color_ref = $wine_text[2];
				 $body_text_color_ref = $wine_text[3];
				 break;
	case "dribble": $bgcolor_reference = $dribble_text[0];
					$main_header_color_ref = $dribble_text[1];
					$sub_header_color_ref = $dribble_text[2];
					$body_text_color_ref = $dribble_text[3];
					break;
}



$bgcolor_referencergb = hex2rgb($bgcolor_reference);
$main_header_color_ref_rgb = hex2rgb($main_header_color_ref);
$sub_header_color_ref_rgb = hex2rgb($sub_header_color_ref);
$body_text_color_ref_rgb = hex2rgb($body_text_color_ref);


if($bgcolor_referencergb[0] + 50 > 255)
	$bgcolor_referencergb[0] = 255;
else
	$bgcolor_referencergb[0]=$bgcolor_referencergb[0]+50;

if($bgcolor_referencergb[1] + 50 > 255)
	$bgcolor_referencergb[1] = 255;
else
	$bgcolor_referencergb[1]=$bgcolor_referencergb[1]+50;
		

if($bgcolor_referencergb[2] + 50 > 255)
	$bgcolor_referencergb[2] = 255;
else
	$bgcolor_referencergb[2]=$bgcolor_referencergb[2]+50;
		
	
$color2=rgb2hex($bgcolor_referencergb);


$gradient_spawn = new gd_gradient_fill($width,100,'vertical',$color2,$bgcolor_reference);
 
 

$bgcolor_referencergb = hex2rgb($bgcolor_reference); 
 
header('content-type: image/png');


//Create background and copy it to the gradient
$gradient = imagecreatefrompng('gradient.png');

$background = imagecreate($width, 300);
$backgroundcolor = imagecolorallocate($background, $bgcolor_referencergb[0], $bgcolor_referencergb[1], $bgcolor_referencergb[2]);
$bgcolor = imagecolorallocate($background, $bgcolor_reference_explode[0], $bgcolor_reference_explode[1], $bgcolor_reference_explode[2]); 
imagecopymerge($background, $gradient, 0, 0, 0, 0, $width, 100, 100);

$main_header_color = imagecolorallocate($background, $main_header_color_ref_rgb[0], $main_header_color_ref_rgb[1], $main_header_color_ref_rgb[2]);
$sub_header_color = imagecolorallocate($background, $sub_header_color_ref_rgb[0], $sub_header_color_ref_rgb[1], $sub_header_color_ref_rgb[2]);
$body_text_color = imagecolorallocate($background, $body_text_color_ref_rgb[0], $body_text_color_ref_rgb[1], $body_text_color_ref_rgb[2]);

$title_font_size = (350/strlen($title))+$title_font_adjuster;
$author_font_size =(180/strlen($author));
if($author_font_size > $max_author_font)
	$author_font_size = $max_author_font;
if($title_font_size > $max_title_font)
	$title_font_size = $max_title_font;
	
imagettftext($background, $title_font_size, 0, 20, 70, $main_header_color, $font_path, $title);
imagettftext($background, $author_font_size, 0, 410+(($author_font_size/strlen($author)))*strlen($author), 22, $sub_header_color, $font_path, $author);
imagettftext($background, 14-$body_font_adjuster, 0, 20, 90, $body_text_color, $font_path, $description_array[0]);
imagettftext($background, 14-$body_font_adjuster, 0, 20, 95+14-$body_font_adjuster, $body_text_color, $font_path, $description_array[1]);
imagettftext($background, 14-$body_font_adjuster, 0, 20, 100+2*(14-$body_font_adjuster), $body_text_color, $font_path, $description_array[2]);
imagettftext($background, 14-$body_font_adjuster, 0, 20, 105+3*(14-$body_font_adjuster), $body_text_color, $font_path, $description_array[3]);

imagepng($background);
imagedestroy($gradient);
?>