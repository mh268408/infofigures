<?php
function percentage($num, $sum)
{
	if($num == 0)
		return 0;
	else{
	$percent = $num/$sum;
	$percent = $percent*360;
	
	if($percent>359)
		$percent = 359;
	
	return $percent;
	}
}

function pie_percentage($num, $sum)
{
	if($num==0)
		return 0;
	else{
		$percent = $num/$sum;
		$percent = $percent*(M_PI*M_PI);
	}
	return $percent;
}
?>