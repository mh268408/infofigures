<?php
	//Included libraries
	require_once("gradient-fill.php"); //needed to make the gradients
	
	//Variables
	$author = "Created by: ".$_POST['author'];
	$title = $_POST['title'];
	$width = $_POST['width'];
	$color1 = "#".$_POST['color1'];
	$color2 = "#".$_POST['color2'];
	$description = $_POST['description'];
	$font_path = "../fonts/Freeroad.ttf";
	$color2_explode = hex2rgb($color2); //Broken up in order to use for color allocation
	$color1_explode = hex2rgb($color1);
	
	if((float)strlen($description) > (.44*$width)){
		$description = "Error: Description length too long";
	}
	
	
	/*Make sure the colors are being exploded properly
	echo($color2_explode[0]."<br>");
	echo($color2_explode[1]."<br>");
	echo($color2_explode[2]);
	*/
	//Spawn the gradient image to be copied onto a background
	$gradient_spawn = new gd_gradient_fill($width,100,'vertical',$color1,$color2);
	
	
	header('content-type: image/png');
	//Create background and copy it to the gradient
	$gradient = imagecreatefrompng('gradient.png');
	$background = imagecreate($width, 300);
	$bgcolor = imagecolorallocate($background, $color2_explode[0], $color2_explode[1], $color2_explode[2]); 
	imagecopymerge($background, $gradient, 0, 0, 0, 0, $width, 100, 100);
	
	//Add some text
	
	if($color1_explode[0]+$color1_explode[1]+$color1_explode[2] < 382){
		$text_color_adjuster = 255;
	}
	else{
		$text_color_adjuster = 0;
	}
	$textcolor = imagecolorallocate($background, $text_color_adjuster, $text_color_adjuster, $text_color_adjuster);
	$authorx = $width - (strlen($author)*9 + 15);
	imagettftext($background, 50, 0, 10, 75, $textcolor, $font_path, $title);
	imagettftext($background, 12, 0, $authorx, 16, $textcolor, $font_path, $author);
	imagettftext($background, 12, 0, 10, 100, $textcolor, $font_path, $description);
	
	
	imagepng($background, "fullheader.png");
	imagedestroy($gradient);
	header("Location: ../index.html");
?>
