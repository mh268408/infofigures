<?php
	/**
	 *Gradient function by http://codingforums.com/showthread.PHP?t=79463 Much thanks to user "Jak-s".
	 *Documentation added by Drew Douglass
	 *
	 *@param string $hexstart is the hex color to start at 
	 *@param string $hexend is the hex color to end at 
	 *@param int steps is the number of steps (or length) of the gradient 
	 *@return array $gradient returns an array of all the color values 
	 *
	 *See bottom for usage. 
	 */
	ini_set("display_errors", "1");
	error_reporting(E_ALL); 
	 
	function gradient($hexstart, $hexend, $steps) {
	 
		//Find the start red, green, and blue values
		//See http://us2.PHP.net/manual/en/function.hexdec.PHP for more info 
		$start['r'] = hexdec(substr($hexstart, 0, 2));
		$start['g'] = hexdec(substr($hexstart, 2, 2));
		$start['b'] = hexdec(substr($hexstart, 4, 2));
	 
		//Find the end of the red, green, and blue values
		$end['r'] = hexdec(substr($hexend, 0, 2));
		$end['g'] = hexdec(substr($hexend, 2, 2));
		$end['b'] = hexdec(substr($hexend, 4, 2));
	 
		//Calculate steps for each color value 
		$step['r'] = ($start['r'] - $end['r']) / ($steps - 1);
		$step['g'] = ($start['g'] - $end['g']) / ($steps - 1);
		$step['b'] = ($start['b'] - $end['b']) / ($steps - 1);
	 
		$gradient = array();
	 
		///Loop through each color depending on steps 
		for($i = 0; $i <= $steps; $i++) {
	 
			$rgb['r'] = floor($start['r'] - ($step['r'] * $i));
			$rgb['g'] = floor($start['g'] - ($step['g'] * $i));
			$rgb['b'] = floor($start['b'] - ($step['b'] * $i));
	 
			$hex['r'] = sprintf('%02x', ($rgb['r']));
			$hex['g'] = sprintf('%02x', ($rgb['g']));
			$hex['b'] = sprintf('%02x', ($rgb['b']));
	 
			$gradient[] = implode(NULL, $hex);
	 
		}
	 
		return $gradient;
	 
	}
	 
	/**
	  *Example usage with a LOT of divs
	  *Dont you ever do this on a clients website ;-) 
	  */
	$gradient = gradient('4096EE', 'C3D9FF', 50);
	 
	foreach($gradient as $color){
		echo '<div style="background-color:'.$color.';">&nbsp;</div>';
	}
?>