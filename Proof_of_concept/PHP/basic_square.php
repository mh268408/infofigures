<?php

	ini_set("display_errors", "1");
	error_reporting(E_ALL);
	
	header('content-type: image/png');
	
	list($month, $day, $year) = explode('/', date('F/jS/Y'));
	
	$image = imagecreatefrompng('../images/calendar_blank.png');
	$image_width = imagesx($image);
	
	$white = imagecolorallocate($image, 255, 255, 255);
	$black = imagecolorallocate($image, 0, 0, 0);
	
	$font_path = '../fonts/Freeroad.ttf';
	
	$pos_month = imagettfbbox(13, 0, $font_path, $month);
	$pos_day = imagettfbbox(15, 0, $font_path, $day);
	$pos_year = imagettfbbox(8, 0, $font_path, $year);

	//Create Month
	imagettftext($image, 13, 0, ($image_width - $pos_month[2]) / 2, 40, $white, $font_path, $month);
	 
	//Create Day 
	imagettftext($image, 15, 0, ($image_width - $pos_day[2]) / 2, 80, $black, $font_path, $day);
	 
	//Create Year
	imagettftext($image, 8, 0, ($image_width - $pos_year[2]) / 2, 100, $black, $font_path, $year);
 
	imagejpeg($image, '', 100);
	
	imagedestroy($image);
?>